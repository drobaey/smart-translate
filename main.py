import pygame
import sys
import speech_recognition as sr
import sounddevice # inutilisé mais l'importer cache les warnings
import threading
import queue 
from time import sleep
import time
import deepl
from gtts import gTTS
import os
import gtts.lang 


# Function to split text into lines that fit within the specified width
def split_text(text, font, max_width):
    words = text.split()
    lines = []
    current_line = ""
    for word in words:
        test_line = current_line + " " + word
        if font.size(test_line)[0] <= max_width:
            current_line = test_line
        else:
            lines.append(current_line)
            current_line = word
    lines.append(current_line)
    return lines


def display_text_on_screen(text, font, height, width, screen, BLACK, WHITE):
    lines = split_text(text, font, width)
    
    first_line_complete = False

    # on réinitialise l'écran tout en noir
    screen.fill(BLACK)
    
    current_line_letter_index = 0
    
    # on affiche le texte sur 2 lignes
    # si on dépasse 2 lignes, on attend 1s, on les efface, puis on affiche la suite
    for i in range(len(lines)):
        for current_line_letter_index in range(1, len(lines[i])+1):
            screen.fill(BLACK)
            
            if not first_line_complete:
                current_line = lines[i][:current_line_letter_index]
                text_surface_line1 = font.render(current_line, True, WHITE)
                text_rect_line1 = text_surface_line1.get_rect(midtop=(width // 2, height // 3))
                screen.blit(text_surface_line1, text_rect_line1)
            else:
                text_surface_line1 = font.render(lines[i-1], True, WHITE)
                text_rect_line1 = text_surface_line1.get_rect(midtop=(width // 2, height // 3))
                screen.blit(text_surface_line1, text_rect_line1)
                
                current_line = lines[i][:current_line_letter_index]
                text_surface_line1 = font.render(current_line, True, WHITE)
                text_rect_line1 = text_surface_line1.get_rect(midtop=(width // 2, height // 3 + 50))
                screen.blit(text_surface_line1, text_rect_line1)

            # mise à jour du display
            pygame.display.flip()

            # temps d'attente avant d'afficher la prochaine lettre
            time.sleep(0.04)
        
        # s'il y a 2 lignes d'afficher et qu'on doit encore en afficher, 
        # on attend 0.5s avant de supprimer les 2 lignes et afficher les nouvelles
        if (first_line_complete and i % 2 == 1):
            time.sleep(1)
            
        first_line_complete = not first_line_complete



def read_audio(text):
    """
        Lit un fichier audio à partir du texte passé en paramètre (text to speech)
    """

    speech = gTTS(text=text, lang="fr", slow=False)

    # enregistrement de l'audio dans un fichier
    speech.save("sample.mp3")

    # lecture de l'audio
    os.system("mpg321 sample.mp3")



def recognize_audio(recognizer, audio_queue):
    # Initialisation Pygame
    pygame.init()

    # Longueur et largeur A MODIFIER selon la taille de l'écran
    width = 800
    height = 200
    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption("Smart Translate")

    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    font = pygame.font.Font(None, 50)
    
    # API DeepL 
    auth_key = "" # L'API key de DeepL à fournir (sinon le code ne fonctionnera pas)
    translator = deepl.Translator(auth_key)
    
    listen_user = True
            
    # Boucle à l'infini sur les enregistrement audio
    while True:
        
        # Attends qu'une nouvelle piste audio soit ajoutée à audio_queue avant de break
        # On boucle également sur les évènements pygame pour que lors d'un clic sur l'écran,
        # on passe en mode "traduire sur le badge les paroles de l'utilisateur" à "traduire
        # et lire sur l'audio de l'ordinateur ce que dit l'interlocuteur"
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    listen_user = not listen_user # écouter l'utilisateur (FR) ou l'interlocuteur (EN)
                   

            if (audio_queue.qsize() != 0):
                audio = audio_queue.get()
                break
            
        # Récupération
        try:
            # on récupère le texte avant l'API google, puis 
            # on traduit le texte en anglais ou en français
            text = None
            #text = "J'suis trop content j'ai réussi à te piéger sur le côté avec le fou je voulais récupérer ton autre fou parce qu'il me bloquait et là quand j'ai vu que tu l'avais bouger"
            
            if listen_user:
                text = recognizer.recognize_google(audio, language = 'fr')
                text = translator.translate_text(text, source_lang="FR", target_lang="EN-US").text
                display_text_on_screen(text, font, height, width, screen, BLACK, WHITE)
            else:
                text = recognizer.recognize_google(audio, language = 'en')
                # EN et pas EN-US car il ne le reconnait pas en source_lang sinon (au dessus, j'ai un warning 
                # si je met pas EN-US, bizzare)
                text = translator.translate_text(text, source_lang="EN", target_lang="FR").text 
                read_audio(text)
             
        except sr.UnknownValueError:
            print("Google Web Speech API could not understand audio")
        except sr.RequestError as e:
            print(f"Could not request results from Google Web Speech API; {e}")
        finally:
            audio_queue.task_done()


def listen_and_add_to_queue(source, recognizer, audio_queue):
    print("Listening...")
        
    while True:
        audio = recognizer.listen(source, phrase_time_limit=4)
        audio_queue.put(audio)


def main():
    recognizer = sr.Recognizer()
    audio_queue = queue.Queue() # queue comportants les différents enregistrement audio, avant reconnaissance vocale
        
    with sr.Microphone() as source:
        recognizer.adjust_for_ambient_noise(source, duration=1)
        
        # thread pour exécuter la reconnaissance vocale en continue en parallèle et à partir de l'enregistrement audio
        threading.Thread(target=recognize_audio, args=(recognizer, audio_queue,), daemon=True).start()

        # on enregistre l'audio en continu
        listen_and_add_to_queue(source, recognizer, audio_queue)
    

if __name__ == "__main__":
    main()